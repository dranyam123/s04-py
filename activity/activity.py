from abc import ABC, abstractclassmethod

class Animal(ABC):
  @abstractclassmethod
  def eat(self):
    pass
  def make_sound(self):
    pass

class Dog(Animal):
  def __init__(self, name, breed, age):
    super().__init__()
    self._name = name
    self._breed = breed
    self._age = age
  def get_name(self):
    print(f"Dog name is {self._name}")
  
  def get_breed(self):
    print(f"Dog breed is {self._breed}")

  def get_age(self):
    print(f"Dog age is {self._age}")

  def set_name(self, name):
    self._name = name
  def set_breed(self, breed):
    self._breed = breed
  def set_age(self, age):
    self._age = age
  def eat(self, food):
    print(f"Eating {food}")
  def make_sound(self):
    print("Woof! Bark! Arf!")
  def call(self):
    print(f"Here {self._name}!")
    
class Cat(Animal):
  def __init__(self, name, breed, age):
    super().__init__()
    self._name = name
    self._breed = breed
    self._age = age
  def get_name(self):
    print(f"Cat name is {self._name}")
  
  def get_breed(self):
    print(f"Cat breed is {self._breed}")

  def get_age(self):
    print(f"Cat age is {self._age}")

  def set_name(self, name):
    self._name = name
  def set_breed(self, breed):
    self._breed = breed
  def set_age(self, age):
    self._age = age
  def eat(self, food):
    print(f"Served {food}")
  def make_sound(self):
    print("Miyaw! Meow! Miiing!")
  def call(self):
    print(f"{self._name}, come here")

dog1 = Dog("Drogo", "Askal", 3);
dog1.eat("Balbacua")
dog1.make_sound()
dog1.call()


cat1 = Cat("Memmeng", "Persian", 1);
cat1.eat("Fish Fillet")
cat1.make_sound()
cat1.call()